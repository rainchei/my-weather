# my-weather

For Tatum Interviewers

The goal is to demonstrate the ability to automate the deployment of a dockerized application using Gitlab CI

# Implementation steps

## 0. Tools in use

- [ ] [awscli](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
- [ ] `terraform`
- [ ] `eksctl`
- [ ] `kubectl`
- [ ] `helm`

## 1. Setup access for AWS CLI

Generate the config and credential files in `~/.aws/config` and `~/.aws/credentials`. Refer to [Configuration and credential file settings](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html) for more.

Check if the current setup is valid. The following command should return the account id and the user arn

```
aws sts get-caller-identity
```

## 2. Data storage on AWS S3

Environment variables (sensitive data are hashed)

```
API_KEY="***************************52198"
LAT="25.0171608"
LON="121.3662944"
MY_BUCKET="my-weather"
FILENAME="daily-220418.json"
```

Create a S3 bucket to store data

```
aws s3 mb s3://$MY_BUCKET
```

Download data from openweather api

```
curl https://api.openweathermap.org/data/2.5/onecall?lat=$LAT&lon=$LON&exclude=current,hourly,minutely&appid=$API_KEY -o /tmp/$FILENAME
```

Upload data to the S3 bucket

```
aws s3 cp /tmp/$FILENAME s3://$MY_BUCKET/
```

## 3. Infra management (AWS VPC/EKS) on Terraform

Environment variables

```
TF_ENV="tatum-dev"
TF_STATE_BUCKET="tatum-dev-tf-state-1650332490"
TF_REGION="ap-northeast-1"
```

Create S3 bucket to store terraform state file

```
aws s3 mb s3://$TF_STATE_BUCKET
```

Change directory to `tf-mgmt`

```
cd tf-mgmt
```

Copy the file `env/my-template.tfvars` to `env/$TF_ENV.tfvars`. In the general section, replace the value of `env`, `s3_state_bucket`, and `region` with the above settings. Or could simply run the following command:

```
sed -i \
  -e 's/env *= ".*"/env = '''\"$TF_ENV\"'''/g' \
  -e 's/s3_state_bucket *= ".*"/s3_state_bucket = '''\"$TF_STATE_BUCKET\"'''/g' \
  -e 's/region *= ".*"/region = '''\"$TF_REGION\"'''/g' \
  env/my-template.tfvars > env/$TF_ENV.tfvars
```

Run the following commands to provision the resources for AWS VPC

```
./deploy.sh init env/$TF_ENV.tfvars vpc
./deploy.sh apply env/$TF_ENV.tfvars vpc
```

Copy the output of AWS VPC, and update the following values accordingly on `env/$TF_ENV.tfvars` in the eks section

- [ ] `vpc_id = eks_vpc_id`
- [ ] `private_subnets = eks_subnet_ids`
- [ ] `default_security_group_id = eks_node_group_additional_sg`

In the eks section on `env/$TF_ENV.tfvars`, update the following values 

- [ ] `eks_aws_auth_roles`
- [ ] `eks_aws_auth_users`

Run the following commands to provision the resources for AWS EKS

```
./deploy.sh init env/$TF_ENV.tfvars eks
./deploy.sh apply env/$TF_ENV.tfvars eks
```

Once the EKS cluster is created, we can setup our `~/.kube/config` by 

```
aws eks update-kubeconfig --name my-kube --region $TF_REGION
```

## 4. Deployments on EKS

Environment variables (sensitive data are hashed)

```
AWS_ACCOUNT="777777777777"
AWS_REGION="ap-northeast-1"
```

Install `aws-load-balancer-controller` to handle K8s Ingress

```
# oidc
eksctl utils associate-iam-oidc-provider \
    --region $AWS_REGION \
    --cluster my-kube \
    --approve

# iam policy
curl -o /tmp/iam-policy.json https://raw.githubusercontent.com/kubernetes-sigs/aws-load-balancer-controller/v2.4.1/docs/install/iam_policy.json
aws iam create-policy \
    --policy-name AWSLoadBalancerControllerIAMPolicy \
    --policy-document file:///tmp/iam-policy.json

# irsa - allow default permissions for aws-load-balancer-controller
eksctl create iamserviceaccount \
    --cluster=my-kube \
    --namespace=kube-system \
    --name=aws-load-balancer-controller \
    --attach-policy-arn=arn:aws:iam::$AWS_ACCOUNT:policy/AWSLoadBalancerControllerIAMPolicy \
    --override-existing-serviceaccounts \
    --region $AWS_REGION \
    --approve

# deploy aws-load-balancer-controller
helm install aws-load-balancer-controller eks/aws-load-balancer-controller \
    -n kube-system \
    --set clusterName=my-kube \
    --set serviceAccount.create=false \
    --set serviceAccount.name=aws-load-balancer-controller
```

Update `ingress.yaml` so that `alb.ingress.kubernetes.io/subnets = <public_subnet_1>,<public_subnet_2>`

The `deploy.yaml` is configured to deploy weather Pods across different nodes to achieve HA

Initial deployment for the weather app

```
# irsa - allow weather app to read S3 bucket
eksctl create iamserviceaccount \
    --cluster=my-kube \
    --namespace=default \
    --name=weather \
    --attach-policy-arn=arn:aws:iam::aws:policy/AmazonS3ReadOnlyAccess \
    --region ap-northeast-1 \
    --approve --override-existing-serviceaccounts

# deploy & ingress
kubectl apply -f deploy.yaml
kubectl apply -f ingress.yaml
```

## 5. GitLab project

Go to this project [my-weather](https://gitlab.com/rainchei/my-weather)

Go to "Settings" -> "CI/CD" -> "Variables". Add the following variables:

- [ ] `CI_REGISTRY = docker.io`
- [ ] `CI_REGISTRY_IMAGE = rainchei/weather`
- [ ] `CI_REGISTRY_USER = ********`
- [ ] `CI_REGISTRY_PASSWORD = **********`

Go to "Settings" -> "Repository" -> "Protected branches". Add the branch `tatum-devops`

Setup GitLab agent, for more detail refer to this [doc](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html)

- [ ] Go to "Infrastructure" -> "Kubernetes clusters" -> "Connect a cluster (agent)"
- [ ] Create a new agent named "tatum-devops"
- [ ] Upon the final step of the registration, use the provided `helm` command to install the agent in the EKS cluster, which is similar to the following

    ```
    helm repo add gitlab https://charts.gitlab.io
    helm repo update
    helm upgrade --install gitlab-agent gitlab/gitlab-agent \
        --namespace gitlab-agent \
        --create-namespace \
        --set config.token=************************************************** \
        --set config.kasAddress=wss://kas.gitlab.com
    ```

Push the code updates or fire a PR to this project to trigger the CI/CD pipeline. The configured `.gitlab-ci.yml` will

- [ ] build docker image and push it to dockerhub
- [ ] update the image tag for the weather app deployment

## 6. Check the result

Get the ALB endpoint and check the daily api

```
EXTERNAL_ALB=$(kubectl get ing external -o=jsonpath='{.status.loadBalancer.ingress[*].hostname}')
curl -H 'Host: weather.example.com' http://$EXTERNAL_ALB/daily
```

## 7. Clean up

```
# weather app
kubectl delete -f deploy.yaml
kubectl delete -f ingress.yaml

# helm
helm uninstall -n kube-system aws-load-balancer-controller
helm uninstall -n gitlab-agent gitlab-agent

# terraform
cd tf-mgmt
./deploy.sh destroy env/$TF_ENV.tfvars eks
./deploy.sh destroy env/$TF_ENV.tfvars vpc

# s3 bucket
aws s3 mb s3://$MY_BUCKET --force
aws s3 mb s3://$TF_STATE_BUCKET --force
```

***

# Problems

- sensitive data stored in `tf-mgmt` (e.g. `env/*.tfvars`)
- security concern for serviceAccount: gitlab-agent? may need to check https://about.gitlab.com/blog/2021/09/10/setting-up-the-k-agent/
- https://github.com/terraform-aws-modules/terraform-aws-eks/issues/1542 which caused error "expect exactly one securityGroup tagged with kubernetes.io/cluster/<cluster-name> for eni" while creating AWS ALB. The workaround is to remove the tag for sg "eks-cluster-sg-*"

# Todos

- it's better to store codes for weather-app (python), tf-mgmt (terraform), and k8s-yaml directories in different gitlab projects
- setup ci/cd for terraform
- setup application cd using helm + argocd (more flexible and more secure) as the way GitOps suggested
- ci triggers for python code/dockerfile updates only
- setup healthcheck for target group "weather"
- setup aws route53 dns record as "weather.example.com" for alb
- setup secret storage service e.g. Hashicorp/Vault to store sensitive env vars
- limit service account for python-app to only take actions such as s3 download
- terraform auto refer e.g. vpc_id as values for provision eks

