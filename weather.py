#!/usr/bin/env python3

from flask import Flask
from os import getenv
from sys import stdout
from datetime import datetime
from prettytable import PrettyTable
import json
import logging
import boto3

# get env
MY_BUCKET = getenv("MY_BUCKET", "my-weather")
FILENAME = getenv("FILENAME", "daily-220418.json")

# log to stdout
logging.basicConfig(stream=stdout, level=logging.INFO)

# download data from s3
s3_resource = boto3.resource("s3")
bucket = s3_resource.Bucket(MY_BUCKET)
logging.info(f"Downloading file ({FILENAME}) from S3 bucket ({MY_BUCKET})")
for obj in bucket.objects.all():
    if obj.key == FILENAME:
        bucket.download_file(obj.key, FILENAME)
logging.info(f"File ({FILENAME}) download completed")
f = open(FILENAME)
data = json.load(f)

# flask
app = Flask(__name__)

@app.route("/")
def healthcheck():
    return "ok"

@app.route("/daily")
def daily():
    x = PrettyTable()
    x.field_names = [str(datetime.fromtimestamp(d["dt"]).date()) for d in data["daily"]]
    rows = [
        [d["weather"][0]["main"] for d in data["daily"]]
    ]
    x.add_rows(rows)

    try:
        return x.get_html_string()
    finally:
        f.close()

# main
app.run(host='0.0.0.0', port=8080)
